import React, { useState } from 'react';
import { SwitcherContainer, SwitcherItem, SwitcherTitle, SwitchMarker } from "./style";

type Item = {
  id: number,
  name: string
}

interface Props {
  onCallbackChange: (index: number) => void;
  items: Item[]
}

const Switcher: React.FunctionComponent<Props> = (props) => {

  const [current, setCurrent] = useState(1);

  const handlerChange = (id: number) => {
    setCurrent(id);
    props.onCallbackChange(id);
  }

  return (
      <SwitcherContainer>
        {props.items.map((item) => {
          return (
              <SwitcherItem
                  key={item.id + '_switch'}
                  isActive={item.id === current}
                  onClick={() => handlerChange(item.id)}
              >
                <SwitcherTitle
                    isActive={item.id === current}
                >
                  {item.name}
                </SwitcherTitle>
              </SwitcherItem>
          )
        })}
        <SwitchMarker isActive={current === 1} />
      </SwitcherContainer>
  );
};

export default Switcher;
