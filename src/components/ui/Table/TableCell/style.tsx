import styled, { css } from "styled-components";
import { PulseBlue, PulseGreen, PulseOrange } from "../../../../styles/animations";

export const TableCellContainer = styled.td<{ showActions?: boolean }>`
  position: relative;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  padding-bottom: 12px;
  padding-top: 12px;
  padding-right: 60px;

  :first-of-type {
    padding-left: 24px;
    padding-right: 0;
  }

  :last-of-type {
    text-align: right;

    padding-right: ${props => props.showActions ? "48px" : "24px"};
  }
`

const statusMixin = css<{ typeStatus?: string }>`
  ${props => props.typeStatus === 'approve' ? 'rgba(58, 212, 150, 0.12)' : 'rgba(255, 181, 70, 0.12)'};
`

const statusMixinColor = css<{ typeStatus?: string }>`
  ${props => props.typeStatus === 'approve' ? '#3AD496' : '#FAB145'};
`

export const TableCellStatus = styled.div<{ typeStatus?: string }>`
  display: inline-block;
  font-size: 14px;
  line-height: 20px;
  color: ${props => props.typeStatus === 'reg' || props.typeStatus === 'decline' ? "#4690FF" : statusMixinColor};
  border-radius: 20px;
  height: 28px;
  padding: 4px 12px;
  background-color: ${props => props.typeStatus === 'reg' || props.typeStatus === 'decline' ? 'rgba(70, 144, 255, 0.12)' : statusMixin};
`

export const TableCellStatusDot = styled.div`
  float: left;
  margin-top: 7px;
  height: 6px;
  width: 6px;
  background: #4690FF;
  border-radius: 50%;
  margin-right: 10px;
  animation: ${PulseBlue} 2s infinite;
`

export const TableCellStatusDotGreen = styled.div`
  float: left;
  margin-top: 7px;
  height: 6px;
  width: 6px;
  background: #3AD496;
  border-radius: 50%;
  margin-right: 10px;
  animation: ${PulseGreen} 2s infinite;
`

export const TableCellStatusDotOrange = styled.div`
  float: left;
  margin-top: 7px;
  height: 6px;
  width: 6px;
  background: #FAB145;
  border-radius: 50%;
  margin-right: 10px;
  animation: ${PulseOrange} 2s infinite;
`
