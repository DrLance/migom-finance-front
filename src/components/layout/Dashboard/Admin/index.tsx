import React, { useEffect, useState } from 'react';
import { DashboardWrapper, FormTitle, HomeFormContainer, ModalLayout } from "../../../../styles";
import Table from "../../../ui/Table";
import DashboardNav from "../Navigation";
import { getLeads, leadChangeStatus } from "../../../../slices/leadtSlice";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { TableActions, TableActionsItem } from "../../../ui/Table/style";
import useComponentVisible from "../../../../constants/hooks";
import { InputLabel } from "../../../ui/Input/style";
import ButtonSubmit from "../../../ui/ButtonSubmit";

const TABLE_TITLES = [
  {
    title: "#",
    field_name: "index"
  },
  {
    title: "Date Referred",
    field_name: "date"
  },
  {
    title: "Company",
    field_name: "company"
  },
  {
    title: "Product",
    field_name: "product"
  }, {
    title: "Status",
    field_name: "status"
  },
  {
    title: "Cost per Lead",
    field_name: "cost"
  },
  {
    title: "",
    field_name: "actions"
  },
]

const DashboardAdmin = () => {
  const dispatch = useDispatch();

  const [actionsObj, setActionsObj] = useState({ x: 0, y: 0, id: 0 });
  const [showStatus, setShowStatus] = useState(false);
  const [currentStatus, setCurrentStatus] = useState('reg');

  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  useEffect(() => {
    dispatch(getLeads());
  }, [dispatch])

  const { data } = useSelector((state: RootState) => state.leads);

  const handleChangeStatus = () => {
    setShowStatus(true)
  }

  const handleChange = async () => {
    await dispatch(leadChangeStatus({status: currentStatus, id: actionsObj.id}));

    setShowStatus(false);
  }

  const handleCurrentStatus = (e: React.SyntheticEvent<HTMLSelectElement>) => {
    const value = e.currentTarget.value;

    setCurrentStatus(value);
  }

  return (
      <div style={{ display: "flex" }}>
        <DashboardNav/>

        <DashboardWrapper style={{ padding: "60px", marginLeft: "0", marginRight: "24px" }}>
          <div style={{ width: "100%", display: "flex", overflow: "auto" }}>
            <Table
                headTitles={TABLE_TITLES}
                tableItems={data ? data : []}
                onClickRow={() => {}}
                onClickCellAction={(e, id) => {
                  const { left, top } = e.currentTarget.getBoundingClientRect();
                  setIsComponentVisible(true);

                  setActionsObj({ x: left, y: top, id });
                }}
            />
          </div>
        </DashboardWrapper>


        <ModalLayout isShow={showStatus}>
          <div style={{ margin: "0 auto" }}>
            <HomeFormContainer>
              <FormTitle>Change agents status</FormTitle>
              <InputLabel>Change to</InputLabel>
              <select style={{ marginBottom: "36px"}} onChange={handleCurrentStatus}>
                <option value="reg">Client started registration</option>
                <option value="approve">Client approved</option>
                <option value="wait">Wayting for payment institution</option>
              </select>

              <ButtonSubmit title="Change" onClick={handleChange} />

            </HomeFormContainer>
          </div>
        </ModalLayout>

        <TableActions
            ref={ref}
            isVisible={isComponentVisible}
            x={actionsObj.x}
            y={actionsObj.y}
        >
          <TableActionsItem onClick={handleChangeStatus}>
            Change lead status
          </TableActionsItem>
        </TableActions>
      </div>
  );
};

export default DashboardAdmin;
