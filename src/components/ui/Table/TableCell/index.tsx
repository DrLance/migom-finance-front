import React from 'react';
import { TableCellContainer, TableCellStatus, TableCellStatusDot, TableCellStatusDotGreen, TableCellStatusDotOrange } from "./style";

interface Props {
  type?: string;
  typeStatus?: string;
  onClick?: () => void;
  isLast?: boolean;
}

const TableCell: React.FunctionComponent<Props> = (props) => {
  return (
      <TableCellContainer onClick={() => {

      }}
      >

        {props.type === 'status' &&
        <TableCellStatus typeStatus={props.typeStatus}>
          {(props.typeStatus === 'reg' || props.typeStatus === 'decline') &&
          <TableCellStatusDot/>
          }
          {props.typeStatus === 'approve' &&
          <TableCellStatusDotGreen />
          }
          {props.typeStatus === 'wait' &&
          <TableCellStatusDotOrange />
          }
          {props.children}
        </TableCellStatus>
        }

        {props.type !== 'status' && props.type !== 'img' &&
        props.children
        }
      </TableCellContainer>
  );
};

export default TableCell;
