export enum Status {
  IDLE = 'idle',
  FAILED = 'failed',
  LOADING = 'loading',
  SUCCEEDED = 'succeeded',
}

export interface LoginData {
  email: string;
  password: string;
}


export interface RegisterData {
  email: string;
  password: string;
  type: number;
  name: string;
}

export type UserInfo = {
  id: number,
  email: string,
  name: string,
  type: number | null,
  is_admin: null | boolean,
  avatar_url: string,
  balance: number,
  ref_link: null | string,
  created_at: string
}

export type UserParamData = {
  type: number,
  ids: Array<number>
}

export type CountryData = {
  id: number,
  name: string,
  code: string,
  icon: string
}

export type ProductData = {
  id: number,
  name: string,
  icon: string
}

export type UserLeadData = {
  ref_id: string | undefined,
  type_lead: number,
  product_id: number  
}


export type AgentLeadData = {
  id: number,
  date: string,
  company: string,
  product: string,
  status: string,
  cost: number
}
