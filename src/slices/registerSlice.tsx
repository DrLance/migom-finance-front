import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { setParamApi } from "../api/user";
import { RegisterData, Status, UserInfo, UserParamData } from "../types";
import { getUserToken } from "../constants/api";
import { registerApi } from "../api/auth";
import { getUser } from "./userSlice";

export interface UserState {
  error: boolean,
  message: string | undefined,
  data: UserInfo,
  status: Status,
  isReg: boolean,
  isSetParam: boolean,
}

const initialState: UserState = {
  status: Status.IDLE,
  error: false,
  message: "",
  isReg: false,
  isSetParam: false,
  data: {
    id: 0,
    email: "",
    name: "",
    type: null,
    is_admin: false,
    created_at: "",
    avatar_url: "",
    ref_link: null,
    balance: 0
  },
};


export const register = createAsyncThunk('register/register', async (data: RegisterData, thunkAPI) => {
  await thunkAPI.dispatch(clearReg());
  return await registerApi(data);
});


export const userSetParam = createAsyncThunk('register/set-param', async (data: UserParamData, thunkAPI) => {
  const token = getUserToken();
  await setParamApi(token, data);
  await thunkAPI.dispatch(clearReg());
  return thunkAPI.dispatch(getUser());
});


export const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    clearReg: (state) => {
      state.isReg = false;
      state.isSetParam = false;
      state.status = Status.IDLE;
    }
  },
  extraReducers: builder => {

    builder.addCase(register.pending, (state) => {
      state.status = Status.LOADING;
      state.isReg = false;
      state.message = "";
    });

    builder.addCase(register.rejected, (state, action) => {
      state.status = Status.FAILED;
      state.isReg = false;
      state.message = action.error.message

      console.log(action);
    });

    builder.addCase(register.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.isReg = true;
      state.data.is_admin = action.payload?.data.is_admin || null;
      state.data.type = action.payload?.data.type || null;
    });

    builder.addCase(userSetParam.pending, (state) => {
      state.status = Status.LOADING;
      state.isSetParam = false;
    });

    builder.addCase(userSetParam.rejected, (state) => {
      state.status = Status.FAILED;
      state.isSetParam = false;
    });

    builder.addCase(userSetParam.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.isSetParam = true;
    });

  }
});

export const { clearReg } = registerSlice.actions;
