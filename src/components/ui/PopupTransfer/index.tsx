import React from 'react';
import { PopupTransferBtnContainer, PopupTransferBtnWrap, PopupTransferName, PopupTransferRow, PopupTransferText, PopupTransferWrapper } from "./style";
import { HomeFormContainer } from "../../../styles";
import ButtonSubmit from "../ButtonSubmit";
import ButtonDecline from "../ButtonDecline";

interface Props {
  onAccept: () => void;
  onDecline: () => void;
  name: string;
  type: string;
  annual?: string;
  service?: string;
}

const PopupTransfer: React.FunctionComponent<Props> = (props) => {
  return (
      <PopupTransferWrapper>
        <HomeFormContainer>

          <div>
            <PopupTransferRow>
              <PopupTransferName>
                Name of the company
              </PopupTransferName>
              <PopupTransferText>
                {props.name}
              </PopupTransferText>
            </PopupTransferRow>
            <PopupTransferRow>
              <PopupTransferName>
                Type of business
              </PopupTransferName>
              <PopupTransferText>
                {props.type}
              </PopupTransferText>
            </PopupTransferRow>
            <PopupTransferRow>
              <PopupTransferName>
                Annual turnover
              </PopupTransferName>
              <PopupTransferText>
                $5.22M
              </PopupTransferText>
            </PopupTransferRow>
            <PopupTransferRow>
              <PopupTransferName>
                Service needed (product)
              </PopupTransferName>
              <PopupTransferText>
                Nursing Assistant
              </PopupTransferText>
            </PopupTransferRow>
          </div>

          <PopupTransferBtnContainer>
            <PopupTransferBtnWrap>
              <ButtonSubmit title="Accept" onClick={props.onAccept}/>
            </PopupTransferBtnWrap>

            <PopupTransferBtnWrap>
              <ButtonDecline title="Decline" onClick={props.onDecline}/>
            </PopupTransferBtnWrap>
          </PopupTransferBtnContainer>

        </HomeFormContainer>
      </PopupTransferWrapper>
  );
};

export default PopupTransfer;
