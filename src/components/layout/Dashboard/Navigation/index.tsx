import React from 'react';
import { AgentIcon, AllLeadsIcon, AllPaymentIcon, DashboardNavContainer, DashboardNavItem, DashboardNavLink, DashboardNavList } from "./style";
import { DASHBOARD, DASHBOARD_ADMIN_AGENTS, DASHBOARD_ADMIN_PAYMENTS } from "../../../../constants/routes";
import { useLocation } from "react-router-dom";

const DashboardNav = () => {

  const { pathname } = useLocation();

  return (
      <DashboardNavContainer>
        <DashboardNavList>
          <DashboardNavItem isHere={DASHBOARD === pathname}>
            <DashboardNavLink to={DASHBOARD} isHere={DASHBOARD === pathname}>
              <AllLeadsIcon />
              All leads
            </DashboardNavLink>
          </DashboardNavItem>
          <DashboardNavItem isHere={DASHBOARD_ADMIN_PAYMENTS === pathname}>
            <DashboardNavLink to={DASHBOARD_ADMIN_PAYMENTS} isHere={DASHBOARD_ADMIN_PAYMENTS === pathname}>
              <AllPaymentIcon />
              Payment systems
            </DashboardNavLink>
          </DashboardNavItem>
          <DashboardNavItem isHere={DASHBOARD_ADMIN_AGENTS === pathname}>
            <DashboardNavLink to={DASHBOARD_ADMIN_AGENTS} isHere={DASHBOARD_ADMIN_AGENTS === pathname}>
              <AgentIcon />
              Agents
            </DashboardNavLink>
          </DashboardNavItem>
        </DashboardNavList>
      </DashboardNavContainer>
  );
};

export default DashboardNav;
