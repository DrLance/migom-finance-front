import React, { useEffect, useState } from 'react';
import {
  MultiSelectContainer,
  MultiSelectedClose, MultiSelectedImg,
  MultiSelectedItem,
  MultiSelectedItems,
  MultiSelectedText,
  MultiSelectInput, MultiSelectList, MultiSelectListItem,
  MultiSelectUpDown
} from "./style";
import { baseUrl } from "../../../constants/api";
import { CountryData } from "../../../types";
import useComponentVisible from "../../../constants/hooks";

interface Props {
  items: CountryData[],
  onCallbackChange: (ids: Array<number>) => void;
}

const MultiSelect: React.FunctionComponent<Props> = (props) => {

  const [tmpItems, setTempItems] = useState(props.items);
  const [selected, setSelected] = useState(new Array(0));

  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  useEffect(() => {
    setTempItems(props.items);
  }, [props.items]);


  const handlerInputFocus = () => {
    setIsComponentVisible(true);
  }

  const handleSelect = (id: number) => {
    let oldSelected = selected;

    if (!oldSelected.includes(id)) {
      oldSelected.push(id)
    } else {
      oldSelected.splice(id, 1);
    }

    setSelected(oldSelected);
    setIsComponentVisible(false);

    props.onCallbackChange(oldSelected);
  }

  const handleRemove = (id: number) => {
    const oldSelected = selected.filter((item) => item !== id);

    setSelected(oldSelected);
    props.onCallbackChange(oldSelected);
  }

  return (
      <MultiSelectContainer ref={ref}>
        <div style={{ width: "100%", position: "relative" }}>
          <MultiSelectInput
              placeholder=""
              onFocus={handlerInputFocus}
              onInput={(e: React.SyntheticEvent<HTMLInputElement>) => {
                const value = e.currentTarget.value || "";
                const re = new RegExp(value, 'gi');

                const newItems = props.items.filter((item) => {
                  if (item.name.match(re)) {
                    return item;
                  }
                  return null;
                });
                setTempItems(newItems);
              }}

          />

          <MultiSelectUpDown
              onClick={() => setIsComponentVisible(true)}
              isOpen={isComponentVisible}
          />

          <MultiSelectList isVisible={isComponentVisible}>
            {tmpItems.map((item) => {
              return (
                  <MultiSelectListItem
                      key={item.id + '_country'}
                      onClick={() => handleSelect(item.id)}
                      isSelected={selected.includes(item.id)}
                  >
                    <MultiSelectedImg src={`${baseUrl}${item.icon}`} alt={item.name}/>
                    <MultiSelectedText>
                      {item.name}
                    </MultiSelectedText>
                  </MultiSelectListItem>
              )
            })}
          </MultiSelectList>

        </div>

        <MultiSelectedItems>
          {selected.map((id) => {
            const findItem = props.items.find((item) => item.id === id)

            if (!findItem) return null;

            return (
                <MultiSelectedItem key={findItem.id} onClick={() => handleRemove(id)}>
                  <MultiSelectedImg src={`${baseUrl}${findItem.icon}`} alt={findItem.name}/>
                  <MultiSelectedText>
                    {findItem.name}
                  </MultiSelectedText>
                  <MultiSelectedClose/>
                </MultiSelectedItem>
            )
          })}

        </MultiSelectedItems>
      </MultiSelectContainer>
  );
};

export default MultiSelect;
