import styled from "styled-components";
import { poppinsFont } from "../../../styles/fonts";
import { black, lightLightBlue } from "../../../styles/colors";

export const PopupTransferWrapper = styled.div`
  margin: 0 auto;
`

export const PopupTransferBtnContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const PopupTransferBtnWrap = styled.div`
  width: 100%;
  margin-right: 24px;
  
  :last-of-type {
    margin-right: 0;
  }
`


export const PopupTransferRow = styled.div`
  display: flex;
  
  :last-of-type {
    margin-bottom: 36px;
  }
`

export const PopupTransferName = styled.p`
  font-family: ${poppinsFont};  
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  margin: 0;
  margin-bottom: 12px;
  margin-right: 24px;
  padding: 0;
  width: 174px;
  color: ${lightLightBlue};
`

export const PopupTransferText = styled.p`
  font-family: ${poppinsFont};  
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  margin: 0;
  margin-bottom: 12px;
  padding: 0;
  width: 174px;
  color: ${black};
`
