import { GlobalStyle } from "./styles";
import { BrowserRouter, Route } from "react-router-dom";
import HomePage from "./pages/Home";
import { Provider, useDispatch } from "react-redux";
import store from "./store";
import { useEffect } from "react";
import RegisterPage from "./pages/Register";
import {
  DASHBOARD, DASHBOARD_ADMIN_AGENTS, DASHBOARD_ADMIN_PAYMENTS,
  DASHBOARD_AGENTS,
  DASHBOARD_PAYMENTS,
  HOME,
  PASSWORD_NEW,
  REGISTER,
  REGISTER_CHOOSE,
  REGISTER_CONFIRM,
  USER_START
} from "./constants/routes";
import RegisterConfirmPage from "./pages/Register/RegisterConfirm";
import PasswordNewPage from "./pages/PasswordNew";
import UserStartPage from "./pages/User/UserStart";
import RegisterChoosePage from "./pages/Register/RegisterChoose";
import DashboardPage from "./pages/Dashboard";
import DashboardAdminPage from "./pages/Dashboard/Admin";
import { getUser } from "./slices/userSlice";
import { getUserToken } from "./constants/api";
import DashboardPaymentsPage from "./pages/Dashboard/Payments";
import DashboardAdminAgentsPage from "./pages/Dashboard/Admin/Agents";
import DashboardAdminPaymentsPage from "./pages/Dashboard/Admin/Payments";


const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const checkExistingUser = async () => {
      if(getUserToken()) {
        dispatch(getUser());
      }
    };
    checkExistingUser();
  }, [dispatch]);

  return (
      <>
        <GlobalStyle/>
        <BrowserRouter>
          <Route exact path={HOME} component={HomePage} />
          <Route exact path={REGISTER} component={RegisterPage} />
          <Route exact path={REGISTER_CONFIRM} component={RegisterConfirmPage} />
          <Route exact path={REGISTER_CHOOSE} component={RegisterChoosePage} />
          <Route exact path={PASSWORD_NEW} component={PasswordNewPage} />
          <Route exact path={USER_START} component={UserStartPage} />
          <Route exact path={DASHBOARD} component={DashboardAdminPage} />
          <Route exact path={DASHBOARD_ADMIN_AGENTS} component={DashboardAdminAgentsPage} />
          <Route exact path={DASHBOARD_ADMIN_PAYMENTS} component={DashboardAdminPaymentsPage} />
          <Route exact path={DASHBOARD_AGENTS} component={DashboardPage} />
          <Route exact path={DASHBOARD_PAYMENTS} component={DashboardPaymentsPage} />
        </BrowserRouter>
      </>
  );
}

const ProviderWrapper: React.FunctionComponent = () => {
  return (
      <Provider store={store}>
        <App/>
      </Provider>
  );
};

export default ProviderWrapper;
