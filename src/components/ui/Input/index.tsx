import React from 'react';
import { InputContainer, InputElement, InputErrorText, InputForgotLink, InputLabel, InputRow } from "./style";

interface Props {
  labelText: string;
  placeholder: string;
  type: string;
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  showForgot?: boolean;
  showForgotCallback?: () => void;
  isError?: boolean
}

const Input: React.FunctionComponent<Props> = (props) => {
  return (
      <InputContainer>
        <InputRow>
          <InputLabel>{props.labelText}</InputLabel>
          {props.showForgot &&
          <InputForgotLink onClick={props.showForgotCallback}>
            Forgot a password?
          </InputForgotLink>
          }
        </InputRow>
        <InputRow>
          <InputElement
              placeholder={props.placeholder}
              onChange={props.onChange}
              type={props.type}
              isError={props.isError}
          />
          {props.isError && <InputErrorText>Please enter {props.labelText.toLowerCase()}</InputErrorText>          }
        </InputRow>
      </InputContainer>
  );
};

export default Input;
