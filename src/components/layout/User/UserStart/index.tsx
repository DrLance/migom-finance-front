import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { FormTitle, HomeFormContainer, ModalLayout } from '../../../../styles';
import Switcher from '../../../ui/Switch';
import ButtonSubmit from '../../../ui/ButtonSubmit';
import { getProducts } from '../../../../slices/productSlice';
import { RootState } from '../../../../store';
import { baseUrl } from '../../../../constants/api';
import { userAddLead } from '../../../../slices/userSlice';
import { UserStartProductItem, UserStartProductText, UserStartProductWrapper } from './style';
// @ts-ignore
import snsWebSdk from '@sumsub/websdk';

const SWITCH_ITEMS = [
  {
    id: 1,
    name: 'Individuals',
  },
  {
    id: 2,
    name: 'Businesses',
  },
];



const UserStart = () => {
  const dispatch = useDispatch();

  let { id } = useParams<{ id: string | undefined }>();

  const [step, setStep] = useState(1);
  const [typeLead, setTypeLead] = useState(1);
  const [product, setProduct] = useState(0);

  const { data } = useSelector((state: RootState) => state.product);
  const { sumsub_token } = useSelector((state: RootState) => state.user);

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const handlerNext = async () => {
    let nextStep = 1 + step;

    if (nextStep >= 3) {
      await dispatch(
        userAddLead({
          product_id: product,
          type_lead: typeLead,
          ref_id: id,
        })
      );
    }

    if(nextStep < 3)
      setStep(nextStep);
  };


  function launchWebSdk(
      apiUrl: string,
      flowName: string,
      accessToken: string,
      applicantEmail: string,
      applicantPhone: string,
      customI18nMessages: string
  ) {
    let snsWebSdkInstance = snsWebSdk
        .Builder(apiUrl, flowName)
        .withAccessToken(accessToken, (newAccessTokenCallback: any) => {
          // Access token expired
          // get a new one and pass it to the callback to re-initiate the WebSDK
          // @ts-ignore
          let newAccessToken = this.newAccessToken(); // get a new token from your backend
          newAccessTokenCallback(newAccessToken);
        })
        .withConf({
          lang: 'en',
          email: applicantEmail,
          phone: applicantPhone,
          i18n: customI18nMessages,
          onMessage: (type: any, payload: any) => {
            // see below what kind of messages the WebSDK generates
            //console.log('WebSDK onMessage', type, payload);

            if(type === "idCheck.applicantStatus") {
              if(payload.reviewStatus === "completed") {
                setTimeout(() => {
                  window.location.reload();
                }, 1500);
              }
            }
          },
          uiConf: {
            customCss: '',
            // URL to css file in case you need change it dynamically from the code
            // the similar setting at Applicant flow will rewrite customCss
            // you may also use to pass string with plain styles `customCssStr:`
          },
          onError: (error: any) => {
            console.error('WebSDK onError', error);
          },
        })
        .build();

    // you are ready to go:
    // just launch the WebSDK by providing the container element for it
    snsWebSdkInstance.launch('#sumsub-websdk-container');
  }

  console.log(sumsub_token);

  if (sumsub_token.token) {
    // @ts-ignore
    launchWebSdk('https://test-api.sumsub.com', 'basic-kyc', sumsub_token.token);
  }

  return (
    <HomeFormContainer id="sumsub-websdk-container">
      {step === 1 && (
        <div>
          <FormTitle style={{ textAlign: 'left' }}>Apply to the top banks worldwide in one click</FormTitle>
          <div style={{ marginBottom: '36px' }}>
            <Switcher items={SWITCH_ITEMS} onCallbackChange={setTypeLead} />
          </div>
        </div>
      )}

      {step === 2 && (
        <div>
          <FormTitle style={{ textAlign: 'left' }}>I want to</FormTitle>
          <UserStartProductWrapper>
            {data.map((item) => {
              return (
                <UserStartProductItem
                  key={item.id + '_product'}
                  onClick={() => setProduct(item.id)}
                  isSelected={item.id === product}>
                  <img src={`${baseUrl}${item.icon}`} alt={item.name} />
                  <UserStartProductText>{item.name}</UserStartProductText>
                </UserStartProductItem>
              );
            })}
          </UserStartProductWrapper>
        </div>
      )}

      {step === 3 && <div /> }

      <div style={{ width: '100%' }}>
        <ButtonSubmit title="Next" onClick={handlerNext} />
      </div>

      <ModalLayout isShow={step === 14}>
        <div style={{ margin: '0 auto' }}>
          <HomeFormContainer>
            <FormTitle style={{ textAlign: 'left' }}>Thank you, your application is under consideration.</FormTitle>
            <ButtonSubmit title="Close" onClick={() => setStep(1)} />
          </HomeFormContainer>
        </div>
      </ModalLayout>
    </HomeFormContainer>
  );
};
export default UserStart;
