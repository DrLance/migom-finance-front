import React, { useState } from 'react';
import { CopySvgImg, CopySvgImgSuc, RefLink, RefLinkContainer, RefLinkCopied, RefLinkText } from "./style";

interface Props {
  link: null | string;
}

const ReferralLink: React.FunctionComponent<Props> = (props) => {

  const [copied, setCopied] = useState(false);

  const handlerCopy = () => {
    navigator.clipboard.writeText('https://migom-finance-front.vercel.app' + props.link || "");
    setCopied(true);

    setTimeout(() => setCopied(false), 2000);
  }

  return (
      <RefLinkContainer isCopied={copied} onClick={handlerCopy}>

        {!copied &&
        <>
          <RefLinkText>
            Referral link:
          </RefLinkText>
          <RefLink>
            {props.link}
            <CopySvgImg/>
          </RefLink>
        </>
        }
        {copied && <>
          <RefLinkCopied>
            <RefLinkText>
              Referral link copy
            </RefLinkText>
          </RefLinkCopied>
          <CopySvgImgSuc />
        </>}

      </RefLinkContainer>
  );
};

export default ReferralLink;
