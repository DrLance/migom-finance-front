import React from 'react';
import { HomeLayout } from "../../styles";
import HeaderHome from "../../components/layout/Header";
import Login from "../../components/layout/Login";
import Footer from "../../components/layout/Footer";

const HomePage = () => {
  return (
      <HomeLayout>
        <HeaderHome/>
        <Login />
        <Footer />
      </HomeLayout>
  );
};

export default HomePage;
