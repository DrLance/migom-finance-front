import React from 'react';
import {
  DashboardHeaderAuthMenu, DashboardHeaderAuthMenuText,
  DashboardHeaderBalance,
  DashboardHeaderContainer,
  DashboardHeaderMenu,
  DashboardHeaderMenuAvatar, DashboardHeaderMenuOpen, DashboardHeaderMenuText,
  DashboardHeaderMoney,
  DashboardHeaderText
} from "./style";
import { Link, useHistory } from "react-router-dom";
import { DASHBOARD, HOME } from "../../../../constants/routes";
import logoImg from "../../../../assets/logo.svg";
import avatarBlankImg from "../../../../assets/avatar-blank.svg";
import logoutIcon from "../../../../assets/logout-icon.svg";
import { useDispatch } from "react-redux";
import { logout } from "../../../../slices/userSlice";
import useComponentVisible from "../../../../constants/hooks";
import ReferralLink from "../../../ui/ReferalLink";

interface Props {
  showRef?: boolean;
  showBalance?: boolean;
  balance?: number;
  refLink: null | string;
  name?: string,
}

const DashboardHeader: React.FunctionComponent<Props> = (props) => {

  const dispatch = useDispatch();
  let history = useHistory();

  const handlerLogout = async () => {
    await dispatch(logout());
    history.push(HOME);
  }

  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  return (
      <DashboardHeaderContainer>

        <div style={{ display: "flex", alignItems: "center" }}>
          <Link to={DASHBOARD}>
            <img src={logoImg} alt="Migom finance"/>
          </Link>
          {props.showRef &&
          <ReferralLink link={props.refLink}/>
          }
        </div>

        <div style={{ display: "flex" }}>
          {props.showBalance &&
          <DashboardHeaderBalance>
            <DashboardHeaderText>
              Balance:
            </DashboardHeaderText>
            <DashboardHeaderMoney>
              $ {props.balance}
            </DashboardHeaderMoney>
          </DashboardHeaderBalance>
          }
          <DashboardHeaderMenu onClick={() => setIsComponentVisible(!isComponentVisible)}>
            <DashboardHeaderMenuAvatar>
              <img src={avatarBlankImg} alt="Avatar"/>
            </DashboardHeaderMenuAvatar>
            <DashboardHeaderMenuText>
              {props.name}
            </DashboardHeaderMenuText>
            <DashboardHeaderMenuOpen isOpen={isComponentVisible}/>
            <DashboardHeaderAuthMenu isVisible={isComponentVisible} onClick={handlerLogout} ref={ref}>
              <DashboardHeaderAuthMenuText>
                <img src={logoutIcon} alt="Logout"/>
                Sign out
              </DashboardHeaderAuthMenuText>
            </DashboardHeaderAuthMenu>
          </DashboardHeaderMenu>
        </div>

      </DashboardHeaderContainer>
  );
};

export default DashboardHeader;
