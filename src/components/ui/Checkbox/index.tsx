import React from 'react';
import { CheckboxInput, CheckboxLbl, CheckboxMark } from "./style";

interface Props {
  onClick: () => void;
  value: boolean
}

const Checkbox: React.FunctionComponent<Props> = (props) => {
  return (
        <CheckboxLbl>
          <CheckboxInput type="checkbox" checked={props.value} defaultChecked={false} onChange={() => {}} />
          <CheckboxMark onClick={props.onClick} />
        </CheckboxLbl>
  );
};

export default Checkbox;
