import React from 'react';
import { TableCellActions, TableContainer } from "./style";
import TableHeadRow from "./TableHead";
import TableCell from "./TableCell";

interface Props {
  showActions?: boolean;
  onClickRow: (item: any) => void;
  onClickCellAction?: (e: React.SyntheticEvent<HTMLDivElement>, id: any) => void;
  headTitles: Array<{
    title: string,
    field_name: string
  }>,
  tableItems: Array<any>
}

const Table: React.FunctionComponent<Props> = (props) => {

  const renderTableCell = (item: any, index: number) => {

    return props.headTitles.map((title) => {

      if (title.field_name === 'index') {
        return (
            <TableCell key={index + Math.random() + '_index'}>
              {index + 1}
            </TableCell>
        )
      }

      if (title.field_name === 'status') {
        return (
            <TableCell typeStatus={item[title.field_name]} type="status" key={index + Math.random() + '_status'}>
              {item[title.field_name] === 'reg' ? "Client started registration" : ""}
              {item[title.field_name] === 'approve' ? "Client approved" : ""}
              {item[title.field_name] === 'wait' ? "Waiting for payment institution" : ""}
              {item[title.field_name] === 'decline' ? "Declined" : ""}
            </TableCell>
        )
      }

      if (title.field_name === 'cost' || title.field_name === 'balance') {
        return (
            <TableCell key={index + Math.random() + "_cost"}>
              $ {item[title.field_name]}
            </TableCell>
        )
      }

      if (title.field_name === 'actions') {
        return (
            <TableCell key={index + Math.random() + "_actions"}>
              <TableCellActions onClick={(e: React.SyntheticEvent<HTMLDivElement>) => {
                if (props.onClickCellAction) {
                  props.onClickCellAction(e, item.id)
                }
              }}
              />
            </TableCell>
        )
      }

      return (
          <TableCell key={index + Math.random() + '_row_row'}>
            {item[title.field_name] || '-'}
          </TableCell>
      )
    })
  }

  return (
      <TableContainer showActions={props.showActions}>
        <thead>
        <tr>
          {props.headTitles.map((head, index) => {
            return (
                <TableHeadRow key={index + '_head_table'}>
                  {head.title}
                </TableHeadRow>
            )
          })}
        </tr>
        </thead>
        <tbody>

        {props.tableItems.map((item, index) => {

          return (
              <tr key={index + '_table_tr'} onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();

                if (event.currentTarget === event.target) props.onClickRow(item);
              }}>
                {renderTableCell(item, index)}
              </tr>
          )

        })
        }

        </tbody>
      </TableContainer>
  );
};

export default Table;
