import React from 'react';
import { CheckboxErrorContainer, CheckboxErrorIcon } from "./style";

const CheckboxError = () => {
  return (
      <CheckboxErrorContainer>
        <CheckboxErrorIcon />
        <p>Сheckbox is not filled</p>
      </CheckboxErrorContainer>
  );
};

export default CheckboxError;
