import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getCountriesApi } from "../api/user";
import { CountryData, Status } from "../types";
import { getUserToken } from "../constants/api";

export interface CountryState {
  error: boolean,
  message: string,
  data: CountryData[],
  status: Status,
}

const initialState: CountryState = {
  status: Status.IDLE,
  error: false,
  message: "",
  data: [{
    id: 0,
    name: "",
    icon: "",
    code: "",
  }],
};


export const getCountries = createAsyncThunk(
    "country/get",
    async () => {
      const token = getUserToken();
      return await getCountriesApi(token);
    }
);

export const countriesSlice = createSlice({
  name: "country",
  initialState,
  reducers: {
  },
  extraReducers: builder => {
    builder.addCase(getCountries.pending, (state) => {
      state.status = Status.LOADING;
    });
    builder.addCase(getCountries.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.data = action.payload;
    });
  }
})


export default countriesSlice.reducer;
