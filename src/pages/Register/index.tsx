import React from 'react';
import { HomeLayout } from "../../styles";
import HeaderHome from "../../components/layout/Header";
import Register from "../../components/layout/Register";
import Footer from "../../components/layout/Footer";

const RegisterPage = () => {
  return (
      <HomeLayout>
        <HeaderHome/>
        <Register/>
        <Footer />
      </HomeLayout>
  );
};

export default RegisterPage;
