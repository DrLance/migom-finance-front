import React, { useState } from 'react';
import { FormTitle, HomeFormContainer } from "../../../styles";
import Input from "../../ui/Input";
import ButtonSubmit from "../../ui/ButtonSubmit";
import { RegisterFooterText, RegisterPrivacyText, RegisterPrivacyWrap } from "./style";
import { InputForgotLink } from "../../ui/Input/style";
import Switcher from "../../ui/Switch";
import { DASHBOARD_AGENTS, HOME, REGISTER_CHOOSE } from "../../../constants/routes";
import { validateEmail } from "../../../constants/heplers";
import { register } from "../../../slices/registerSlice";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router-dom";
import { RootState } from "../../../store";
import Checkbox from "../../ui/Checkbox";
import { Status } from "../../../types";

const SWITCH_ITEMS = [
  {
    id: 1,
    name: "Agents",
  },
  {
    id: 2,
    name: "Payment system",
  }
]

const Register = () => {
  const dispatch = useDispatch();
  let history = useHistory();

  const { data: { type }, status, isReg, message } = useSelector((state: RootState) => state.register);

  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
    name: "",
    type: 1,
    isEmailValid: true,
    isPasswordValid: true,
    isNameValid: true,
    isCheck: false
  });

  const handlerRegister = () => {

    if (!validateEmail(loginData.email)) {
      setLoginData(() => ({
            ...loginData,
            isEmailValid: false,
          })
      );
    }

    if (loginData.password.length <= 0) {
      setLoginData(() => ({
            ...loginData,
            isPasswordValid: false,
          })
      );
    }

    if (loginData.name.length <= 0) {
      setLoginData(() => ({
            ...loginData,
            isNameValid: false,
          })
      );
    }

    if (validateEmail(loginData.email) && loginData.password.length > 0 && loginData.name.length > 0 && loginData.isCheck) {
      dispatch(register({
        email: loginData.email,
        password: loginData.password,
        type: loginData.type,
        name: loginData.name
      }));
    }
  }

  const handleInputChange = (type: string, e: React.FormEvent<HTMLInputElement>) => {

    const value = e.currentTarget.value

    if (type === 'password') {
      setLoginData(() => ({
            ...loginData,
            password: value,
            isPasswordValid: true,
          })
      );
    }

    if (type === 'email') {

      setLoginData(() => ({
            ...loginData,
            email: value,
            isEmailValid: true,
          })
      );
    }

    if (type === 'name') {
      setLoginData(() => ({
            ...loginData,
            name: value,
            isNameValid: true,
          })
      );
    }
  }

  const handlerSwitch = (type: number) => {
    setLoginData(() => ({
          ...loginData,
          type,
        })
    );
  }

  const handlerCheck = () => {
    setLoginData(() => ({
          ...loginData,
          isCheck: !loginData.isCheck,
        })
    );
  }

  if (isReg) {

    if (type === 1) return <Redirect to={DASHBOARD_AGENTS}/>

    if (type === 2) return <Redirect to={REGISTER_CHOOSE}/>

  }

  return (
      <HomeFormContainer style={{ marginTop: "41px" }}>
        <FormTitle>Create Account</FormTitle>

        <div style={{ width: "100%", marginBottom: "24px" }}>
          <Switcher
              onCallbackChange={handlerSwitch}
              items={SWITCH_ITEMS}
          />
        </div>

        <div style={{ width: "100%", marginBottom: "24px" }}>
          <Input
              type="email"
              onChange={(e: React.FormEvent<HTMLInputElement>) => handleInputChange('email', e)}
              placeholder=""
              labelText="Email"
              isError={!loginData.isEmailValid}
          />
        </div>

        <div style={{ width: "100%", marginBottom: "24px" }}>
          <Input
              type="text"
              onChange={(e: React.FormEvent<HTMLInputElement>) => handleInputChange('name', e)}
              placeholder=""
              labelText="Full name"
              isError={!loginData.isNameValid}
          />
        </div>

        <div style={{ width: "100%", marginBottom: "36px" }}>
          <Input
              type="password"
              onChange={(e: React.FormEvent<HTMLInputElement>) => handleInputChange('password', e)}
              placeholder=""
              labelText="Password"
              isError={!loginData.isPasswordValid}
          />
        </div>

        <RegisterPrivacyWrap>
          <Checkbox onClick={handlerCheck} value={loginData.isCheck}/>
          <RegisterPrivacyText>Get emails from Deronica about product updates, industry news, and events.
            If you change your mind, you can unsubscribe at any time. <InputForgotLink onClick={() => history.push("/privacy")}>Privacy Policy</InputForgotLink>
          </RegisterPrivacyText>
        </RegisterPrivacyWrap>

        {(!loginData.isCheck || !loginData.isNameValid || !loginData.isEmailValid || !loginData.isPasswordValid) && null}

        {status === Status.FAILED && <p style={{ marginBottom: "24px", marginTop: 0, color: "red" }}>{message}</p>}

        <div style={{ width: "100%", marginBottom: "36px" }}>
          <ButtonSubmit
              title="Create account"
              onClick={handlerRegister}
              isDisable={(!loginData.isCheck || !loginData.isNameValid || !loginData.isEmailValid || !loginData.isPasswordValid) }
          />
        </div>

        <div style={{ display: "flex" }}>
          <RegisterFooterText>
            Don’t have account?
          </RegisterFooterText>
          <InputForgotLink
              onClick={() => history.push(HOME)}
          >
            Log In
          </InputForgotLink>
        </div>
      </HomeFormContainer>
  );
}

export default Register;
