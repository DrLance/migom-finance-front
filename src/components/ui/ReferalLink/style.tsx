import styled, { keyframes } from "styled-components";
import { poppinsFont } from "../../../styles/fonts";
import { blue, grey, lightLightBlue } from "../../../styles/colors";
import Icon from "../Icon";


const Svg = styled(Icon)`
  height: 16px;
  width: 16px;
  margin-left: 6px;
  fill: ${blue};
`

export const CopySvgImg = () => (
    <Svg viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd"
            d="M5.99994 0.666687H10.9426L13.9999 3.72402V11.3334C13.9999 12.0687 13.4019 12.6667 12.6666 12.6667H5.99994C5.2646 12.6667 4.6666 12.0687 4.6666 11.3334V2.00002C4.6666 1.26535 5.2646 0.666687 5.99994 0.666687ZM5.99994 11.3334H12.6673L12.6666 4.66669H9.99994V2.00002H5.99994V11.3334ZM2 3.33334H3.33333V14H11.3333V15.3333H3.33333C2.598 15.3333 2 14.7353 2 14V3.33334Z"/>
      <path
          d="M10.9426 0.666687L11.0487 0.560621L11.0047 0.516687H10.9426V0.666687ZM13.9999 3.72402H14.1499V3.66189L14.106 3.61795L13.9999 3.72402ZM12.6673 11.3334V11.4834H12.8173L12.8173 11.3333L12.6673 11.3334ZM5.99994 11.3334H5.84994V11.4834H5.99994V11.3334ZM12.6666 4.66669L12.8166 4.66667L12.8166 4.51669H12.6666V4.66669ZM9.99994 4.66669H9.84994V4.81669H9.99994V4.66669ZM9.99994 2.00002H10.1499V1.85002H9.99994V2.00002ZM5.99994 2.00002V1.85002H5.84994V2.00002H5.99994ZM3.33333 3.33334H3.48333V3.18334H3.33333V3.33334ZM2 3.33334V3.18334H1.85V3.33334H2ZM3.33333 14H3.18333V14.15H3.33333V14ZM11.3333 14H11.4833V13.85H11.3333V14ZM11.3333 15.3333V15.4833H11.4833V15.3333H11.3333ZM10.9426 0.516687H5.99994V0.816687H10.9426V0.516687ZM14.106 3.61795L11.0487 0.560621L10.8365 0.772753L13.8939 3.83009L14.106 3.61795ZM14.1499 11.3334V3.72402H13.8499V11.3334H14.1499ZM12.6666 12.8167C13.4848 12.8167 14.1499 12.1515 14.1499 11.3334H13.8499C13.8499 11.9858 13.3191 12.5167 12.6666 12.5167V12.8167ZM5.99994 12.8167H12.6666V12.5167H5.99994V12.8167ZM4.5166 11.3334C4.5166 12.1515 5.18176 12.8167 5.99994 12.8167V12.5167C5.34745 12.5167 4.8166 11.9858 4.8166 11.3334H4.5166ZM4.5166 2.00002V11.3334H4.8166V2.00002H4.5166ZM5.99994 0.516687C5.18171 0.516687 4.5166 1.18256 4.5166 2.00002H4.8166C4.8166 1.34815 5.34749 0.816687 5.99994 0.816687V0.516687ZM12.6673 11.1834H5.99994V11.4834H12.6673V11.1834ZM12.5166 4.6667L12.5173 11.3334L12.8173 11.3333L12.8166 4.66667L12.5166 4.6667ZM9.99994 4.81669H12.6666V4.51669H9.99994V4.81669ZM9.84994 2.00002V4.66669H10.1499V2.00002H9.84994ZM5.99994 2.15002H9.99994V1.85002H5.99994V2.15002ZM6.14994 11.3334V2.00002H5.84994V11.3334H6.14994ZM3.33333 3.18334H2V3.48334H3.33333V3.18334ZM3.48333 14V3.33334H3.18333V14H3.48333ZM11.3333 13.85H3.33333V14.15H11.3333V13.85ZM11.4833 15.3333V14H11.1833V15.3333H11.4833ZM3.33333 15.4833H11.3333V15.1833H3.33333V15.4833ZM1.85 14C1.85 14.8182 2.51516 15.4833 3.33333 15.4833V15.1833C2.68084 15.1833 2.15 14.6525 2.15 14H1.85ZM1.85 3.33334V14H2.15V3.33334H1.85Z"
          fill="white"/>
    </Svg>
)

export const CopySvgImgSuc = () => (
    <Svg viewBox="0 0 16 16">
      <path
          d="M7.46575 12.1843C7.06892 12.6215 6.38169 12.6215 5.98485 12.1843L2.61009 8.46638C2.26402 8.08513 2.26402 7.50341 2.61009 7.12216L3.06197 6.62432C3.45881 6.18714 4.14603 6.18714 4.54287 6.62433L6.57721 8.8655C6.65658 8.95294 6.79402 8.95294 6.87339 8.8655L11.4571 3.81573C11.854 3.37855 12.5412 3.37855 12.938 3.81573L13.3899 4.31356C13.736 4.69482 13.736 5.27654 13.3899 5.65779L7.46575 12.1843Z"
          fill="#253B86" stroke="white"/>
    </Svg>

)

const Fade = keyframes`
  0% { opacity: 0; }
  30% { opacity: 0.3; }
  70% { opacity: 0.7; }
  100% { opacity: 1;}
`;

export const RefLinkContainer = styled.div<{ isCopied?: boolean }>`
  display: flex;
  align-items: center;
  background: #FFFFFF;
  box-shadow: 2px 2px 8px rgba(37, 59, 134, 0.1);
  border-radius: 100px;
  height: 46px;
  margin-left: 60px;
  padding: ${props => props.isCopied ? "0 6px" : "0 16px" } ;
  box-sizing: border-box;
  border: 1px solid transparent;


  :hover {
    cursor: pointer;
    border: 1px solid #E0E8FE;

    svg {
      fill: ${lightLightBlue};
    }
  }
`

export const RefLinkText = styled.p`
  font-family: ${poppinsFont};
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${grey};
  padding: 0;
  margin: 0;
  margin-right: 6px;
  animation: ${Fade};
  animation-duration: 0.15s;
  animation-fill-mode: forwards;  
`

export const RefLink = styled.a`
  display: flex;
  align-items: center;
  font-family: ${poppinsFont};
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${blue};
  animation: ${Fade};
  animation-duration: 0.25s;
  animation-fill-mode: forwards;
`


export const RefLinkCopied = styled.a`
  display: flex;
  align-items: center;
  background-color: #253B86;
  border-radius: 20px;
  padding: 6px 16px;
  height: 32px;
  animation: ${Fade};
  animation-duration: 0.15s;
  animation-fill-mode: forwards;
  
  p {
    color: #ffffff;
  }
`


