import React, { useState } from 'react';
import { FormTitle, HomeFormContainer, ModalLayout } from "../../../styles";
import Input from "../../ui/Input";
import ButtonSubmit from "../../ui/ButtonSubmit";
import { LoginFooterText } from "./style";
import { InputForgotLink } from "../../ui/Input/style";
import ResetPassword from "../ResetPassword";
import { Redirect, useHistory } from "react-router-dom";
import { DASHBOARD, DASHBOARD_AGENTS, DASHBOARD_PAYMENTS, REGISTER } from "../../../constants/routes";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../../slices/userSlice";
import { validateEmail } from "../../../constants/heplers";
import { RootState } from "../../../store";
import { Status } from "../../../types";

const Login: React.FunctionComponent = () => {

  let history = useHistory();
  let dispatch = useDispatch();

  const { isAuth, data: {type, is_admin}, status } = useSelector((state: RootState) => state.user);

  const [showModal, setShowModal] = useState(false);
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
    isEmailValid: true,
    isPasswordValid: true,
  });

  const handlerClose = () => {
    setShowModal(false);
  }

  const handlerLogin = () => {

    if (!validateEmail(loginData.email)) {
      setLoginData(() => ({
            ...loginData,
            isEmailValid: false,
          })
      );
    }

    if(loginData.password.length <= 0) {
      setLoginData(() => ({
            ...loginData,
            isPasswordValid: false,
          })
      );
    }

    if(validateEmail(loginData.email) && loginData.password.length > 0) {
      dispatch(login({
        email: loginData.email,
        password: loginData.password
      }));
    }
  }

  const handleInputChange = (type: string, e: React.FormEvent<HTMLInputElement>) => {

    const value = e.currentTarget.value

    if (type === 'password') {
      setLoginData(() => ({
            ...loginData,
            password: value,
            isPasswordValid: true,
          })
      );
    }

    if (type === 'email') {
      setLoginData(() => ({
            ...loginData,
            email: value,
            isEmailValid: true,
          })
      );
    }
  }

  if(isAuth) {
    if(is_admin) {
      return <Redirect to={DASHBOARD} />
    }

    if(type === 1) {
      return <Redirect to={DASHBOARD_AGENTS} />
    }

    if(type === 2) {
      return <Redirect to={DASHBOARD_PAYMENTS} />
    }
  }

  return (
      <HomeFormContainer>
        <FormTitle>Log in</FormTitle>
        <div style={{ width: "100%", marginBottom: "24px" }}>
          <Input
              type="email"
              onChange={(e: React.FormEvent<HTMLInputElement>) => handleInputChange('email', e)}
              placeholder=""
              labelText="Email"
              isError={!loginData.isEmailValid}
          />
        </div>
        <div style={{ width: "100%", marginBottom: "36px" }}>
          <Input
              type="password"
              onChange={(e: React.FormEvent<HTMLInputElement>) => handleInputChange('password', e)}
              placeholder=""
              labelText="Password"
              showForgot
              showForgotCallback={() => setShowModal(true)}
              isError={!loginData.isPasswordValid}
          />
        </div>

        {status === Status.FAILED && <p style={{ marginBottom: "24px", marginTop: 0, color: "red"}}>Login Failed</p>}

        <div style={{ width: "100%", marginBottom: "36px" }}>
          <ButtonSubmit
              title="Log In"
              onClick={handlerLogin}
              isDisable={!loginData.isEmailValid || !loginData.isPasswordValid}
          />
        </div>

        <div style={{ display: "flex" }}>
          <LoginFooterText>
            Don’t have account?
          </LoginFooterText>
          <InputForgotLink onClick={() => history.push(REGISTER)}>
            Sign Up
          </InputForgotLink>
        </div>

        <ModalLayout isShow={showModal}>
          <ResetPassword onClose={handlerClose}/>
        </ModalLayout>

      </HomeFormContainer>
  );
};

export default Login;
