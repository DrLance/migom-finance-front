import styled, { createGlobalStyle } from "styled-components";
import { globalFont, poppinsFont } from "./fonts";
import bgHomeRect from "../assets/bg-down-rect.svg";
import { black, blue, lightLightBlue } from "./colors";
import { Fade } from "./animations";

export const GlobalStyle = createGlobalStyle`
  html {
    display: block;
    box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: ${globalFont};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background: linear-gradient(0deg, #FFFFFF, #FFFFFF), #FFFFFF;
  }

  * {
    box-sizing: border-box;
  }

  h1 {
    font-weight: 500;
    font-size: 28px;
    line-height: 38px;
  }

  a, button {
    cursor: pointer;
  }
`


export const HomeLayout = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  width: 100%;
  margin: 0 auto;
  background-image: url("${bgHomeRect}");
  background-repeat: no-repeat;
  background-size: cover; 
  background-position: center;    
`

export const HomeFormContainer = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  min-width: 482px;
  max-width: 482px;
  background: #FFFFFF;
  box-shadow: 6px 6px 24px rgba(37, 59, 134, 0.11);
  border-radius: 12px;
  margin: 0 auto;
  margin-top: 142px;
  padding: 60px;
  
  select {
    font-family: ${poppinsFont};
    font-size: 14px;
    line-height: 20px;
    color: #1A1E5D;
    width: 100%;
    height: 46px;
    padding: 13px 16px;
    border: 1px solid #7F7F7F;
    box-sizing: border-box;
    border-radius: 6px;    
    outline: none;
  }
`

export const FormTitle = styled.h2`
  font-family: ${globalFont};
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  text-align: center;
  color: ${blue};
  margin: 0;
  margin-bottom: 36px;
  padding: 0;
`

export const FormSubTitle = styled.h2`
  font-family: ${poppinsFont};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${lightLightBlue};
  margin-top: -30px;
  margin-bottom: 24px;
`

export const ModalLayout = styled.div<{ isShow: boolean }>`
  position: fixed;
  top: 0;
  left: 0;
  align-items: center;
  justify-content: center;
  display: ${props => props.isShow ? "flex" : "none"};  
  height: 100vh;
  width: 100vw;
  background-color: rgba(23, 23, 23, 0.8);
  animation: ${Fade};
  animation-duration: 0.15s;
  animation-fill-mode: forwards;
  z-index: 2;
  
  div:nth-child(1) {
      margin-top: 0!important;    
  }
  
`

export const Link = styled.a`
  font-family: ${poppinsFont};  
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${blue};
`

export const DashboardLayout = styled.div`
  display: flex;  
  background: #E9EBF3;
  height: 100vh;
  width: 100vw;
`

export const DashboardContent = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1456px;
  margin: 0 auto;
  width: 100%;
`

export const DashboardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: #FFFFFF;
  box-shadow: 6px 6px 24px rgba(37, 59, 134, 0.11);
  border-radius: 12px;
  height: 852px;    
  margin-left: 24px;
  width: calc(100% - 48px);
  padding-top: 74px;
  padding-left: 178px;
  padding-right: 218px;
`

export const DashboardTitle = styled.h1`
  font-family: ${globalFont};
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  color: ${black};
  margin: 0;
  margin-bottom: 36px;
  padding: 0;
`

export const TableWrapper = styled.div`
  display: flex;
  overflow: auto;
  position: relative;
  margin-bottom: 104px;
  
  &::-webkit-scrollbar {
    background: #E9EBF3;    
    height: 4px;
  }

  &::-webkit-scrollbar-thumb {
    background: rgba(37, 59, 134, 0.2);    
    height: 6px;
  }
`
