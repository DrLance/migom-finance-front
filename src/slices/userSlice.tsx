import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  adminGetAgentsApi,
  adminGetPaymentSystemsApi,
  ChangeBalance,
  ChangeStatus,
  getUserApi,
  userAddLeadApi,
  userChangeBalanceApi,
  userChangeStatusApi
} from "../api/user";
import { LoginData, Status, UserInfo, UserLeadData } from "../types";
import { getUserToken } from "../constants/api";
import { loginApi, logoutApi } from "../api/auth";
import { clearReg } from "./registerSlice";


export interface UserState {
  error: boolean,
  message: string,
  data: UserInfo,
  admin_data: any,
  status: Status,
  isAuth: boolean,
  status_balance: Status,
  status_change: Status,
  sumsub_token: { token: string, userId: string }
}

const initialState: UserState = {
  status: Status.IDLE,
  error: false,
  message: "",
  isAuth: false,
  data: {
    id: 0,
    email: "",
    name: "",
    type: null,
    is_admin: false,
    created_at: "",
    avatar_url: "",
    ref_link: null,
    balance: 0
  },
  admin_data: [],
  status_balance: Status.IDLE,
  status_change: Status.IDLE,
  sumsub_token: {token: "", userId: ""}
};


export const getUser = createAsyncThunk("user/get",
    async () => {
      const token = getUserToken();
      return await getUserApi(token);
    }
);


export const login = createAsyncThunk('user/login', async (data: LoginData, { dispatch }) => {
  const response = await loginApi(data);
  await dispatch(getUser());
  return response;
});


export const logout = createAsyncThunk('user/logout', async (_arg, { dispatch }) => {
  const token = getUserToken();
  await dispatch(clearReg());
  return await logoutApi(token);
});

export const userAddLead = createAsyncThunk('user/add-lead', async (data: UserLeadData) => {
  return await userAddLeadApi(data);
});

export const adminGetAgents = createAsyncThunk('user/admin-get-agents', async () => {
  const token = getUserToken();
  return await adminGetAgentsApi(token);
});

export const adminGetPaymentSystems = createAsyncThunk('user/admin-get-payment-systems', async () => {
  const token = getUserToken();
  return await adminGetPaymentSystemsApi(token);
});

export const userChangeBalance = createAsyncThunk('user/admin-change-balance', async (data: ChangeBalance) => {
  const token = getUserToken();
  return await userChangeBalanceApi(token, data);
});

export const userChangeStatus = createAsyncThunk('user/admin-change-status', async (data: ChangeStatus) => {
  const token = getUserToken();
  return await userChangeStatusApi(token, data);
});


export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: builder => {

    builder.addCase(getUser.rejected, (state, action) => {
      state.isAuth = false;
    });

    builder.addCase(getUser.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.data = action.payload;
      state.isAuth = true;
    });

    builder.addCase(login.rejected, (state, action) => {
      state.status = Status.FAILED;
    });

    builder.addCase(login.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.isAuth = true;
      state.data.is_admin = action.payload?.data.is_admin || null;
      state.data.type = action.payload?.data.type || null;
    });

    builder.addCase(logout.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.isAuth = false;
      state.data = initialState.data;
    });

    builder.addCase(adminGetAgents.fulfilled, (state, action) => {
      state.admin_data = action.payload;
    });

    builder.addCase(adminGetPaymentSystems.fulfilled, (state, action) => {
      state.admin_data = action.payload;
    });

    builder.addCase(userChangeBalance.fulfilled, (state, action) => {
      state.status_balance = Status.SUCCEEDED;
    });

    builder.addCase(userChangeStatus.pending, (state, action) => {
      state.status_balance = Status.LOADING;
    });

    builder.addCase(userChangeStatus.fulfilled, (state, action) => {
      state.status_balance = Status.SUCCEEDED;
    });

    builder.addCase(userAddLead.fulfilled, (state, action) => {
      state.sumsub_token = action.payload?.sumsub_token;
    });

  }
})


export default userSlice.reducer;
