export const blue = "#1A1E5D";
export const lightBlue = "#E0E8FE";
export const orange = "#FE5728";
export const black = "#171717";
export const grey = "#7F7F7F";
export const lightLightBlue = "#929DC2";
export const redColor = "#FE2828";
