import React, { useEffect } from 'react';
import { DashboardTitle, DashboardWrapper } from "../../../../styles";
import Table from "../../../ui/Table";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { getLeads } from "../../../../slices/leadtSlice";
import { getUser } from "../../../../slices/userSlice";

const TABLE_TITLES = [
  {
    title: "#",
    field_name: "index"
  },
  {
    title: "Date Referred",
    field_name: "date"
  },
  {
    title: "Company",
    field_name: "company"
  },
  {
    title: "Product",
    field_name: "product"
  }, {
    title: "Status",
    field_name: "status"
  },
  {
    title: "Cost per Lead",
    field_name: "cost"
  },
]

const DashboardAgents = () => {
  const dispatch = useDispatch();

  const { data } = useSelector((state: RootState) => state.leads);

  useEffect(() => {
    dispatch(getLeads());
    dispatch(getUser());
  }, [dispatch])

  return (
      <DashboardWrapper>

        <DashboardTitle>
          Your leads
        </DashboardTitle>

        <div style={{display: "flex", overflow: "auto", marginBottom: "104px"}}>
          <Table
              onClickRow={() => {}}
              headTitles={TABLE_TITLES}
              tableItems={data ? data : []}
          />
        </div>

      </DashboardWrapper>
  );
};

export default DashboardAgents;
