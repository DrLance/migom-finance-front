import React from 'react';
import { Link } from 'react-router-dom';
import { HeaderContainer, HeaderLogo } from "./style";

const HeaderHome = () => {
  return (
      <HeaderContainer>
        <Link to="/">
          <HeaderLogo/>
        </Link>
      </HeaderContainer>
  );
};

export default HeaderHome;
