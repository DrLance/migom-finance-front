import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getUserLeadsApi, getUserPaymentLeadsApi, leadChangeStatusApi } from "../api/user";
import { AgentLeadData, Status } from "../types";
import { getUserToken } from "../constants/api";

export interface CountryState {
  error: boolean,
  message: string,
  data?: AgentLeadData[],
  data_payments?: AgentLeadData[],
  status: Status,
  change_status: Status
}

const initialState: CountryState = {
  status: Status.IDLE,
  error: false,
  message: "",
  data: [],
  data_payments: [],
  change_status: Status.IDLE
};


export const getLeads = createAsyncThunk(
    "leads/get-agent",
    async () => {
      const token = getUserToken();

      return await getUserLeadsApi(token);
    }
);

export const getLeadsPayments = createAsyncThunk(
    "leads/get-payments",
    async () => {
      const token = getUserToken();

      return await getUserPaymentLeadsApi(token);
    }
);

type ChangeLead = {
  status: string,
  id: number | string,
}

export const leadChangeStatus = createAsyncThunk(
    "leads/change-status",
    async (data: ChangeLead, { dispatch }) => {
      const token = getUserToken();
      const response = await leadChangeStatusApi(token, data.status, data.id);

      await dispatch(getLeads());

      return response;
    }
);

export const leadSlice = createSlice({
  name: "leads",
  initialState,
  reducers: {
    clearLeadStatus: (state) => {
      state.change_status = Status.IDLE;
    }
  },
  extraReducers: builder => {
    builder.addCase(getLeads.rejected, (state, action) => {
      state.status = Status.FAILED;
      state.error = true;
    });
    builder.addCase(getLeads.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.error = false;
      state.data = action.payload;
    });

    builder.addCase(getLeadsPayments.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.error = false;
      state.data_payments = action.payload;
    });

    builder.addCase(leadChangeStatus.rejected, (state, action) => {
      state.status = Status.FAILED;
      state.error = true;
    });

    builder.addCase(leadChangeStatus.fulfilled, (state, action) => {
      state.status = Status.SUCCEEDED;
      state.change_status = Status.SUCCEEDED;
      state.error = false;
    });
  }
})

export const { clearLeadStatus } = leadSlice.actions;


export default leadSlice.reducer;
