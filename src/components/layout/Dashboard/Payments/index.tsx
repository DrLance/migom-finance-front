import React, { useEffect, useState } from 'react';
import { DashboardTitle, DashboardWrapper, FormTitle, HomeFormContainer, ModalLayout, TableWrapper } from "../../../../styles";
import Table from "../../../ui/Table";
import PopupTransfer from "../../../ui/PopupTransfer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { clearLeadStatus, getLeadsPayments, leadChangeStatus } from "../../../../slices/leadtSlice";
import { Status } from "../../../../types";
import ButtonSubmit from "../../../ui/ButtonSubmit";


const TABLE_TITLES = [
  {
    title: "#",
    field_name: "index"
  },
  {
    title: "Date Referred",
    field_name: "date"
  },
  {
    title: "Name",
    field_name: "name"
  },
  {
    title: "Country",
    field_name: "country"
  }, {
    title: "Age of Business",
    field_name: "age_business"
  },
  {
    title: "Product",
    field_name: "product"
  },
  {
    title: "Type of business",
    field_name: "type_business"
  },
  {
    title: "Annual turnover",
    field_name: "annual"
  },
  {
    title: "",
    field_name: "actions"
  },
];

const DashboardPayments = () => {
  const dispatch = useDispatch();

  const [showTransfer, setShowTransfer] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  const { data_payments, change_status } = useSelector((state: RootState) => state.leads);

  useEffect(() => {
    dispatch(getLeadsPayments());
  }, [dispatch]);

  const handlerAccept = async () => {
    await dispatch(leadChangeStatus({status: "approve", id: selectedId}))
    setShowTransfer(false);
  }

  const handlerDecline = async () => {
    await dispatch(leadChangeStatus({status: "decline", id: selectedId}))
    setShowTransfer(false);
  }

  const handlerCloseAccept = async () => {
    await dispatch(clearLeadStatus());
  }


  return (
      <DashboardWrapper>

        <DashboardTitle>
          Your leads
        </DashboardTitle>

        <TableWrapper>

          <Table
              onClickRow={() => {}}
              headTitles={TABLE_TITLES}
              tableItems={data_payments ? data_payments : []}
              onClickCellAction={(e, id) => {
                setSelectedId(id);
                setShowTransfer(true);
              }}
          />

        </TableWrapper>

        <ModalLayout isShow={showTransfer}>
          <PopupTransfer
              name="1"
              type="2"
              onAccept={handlerAccept}
              onDecline={handlerDecline}
          />
        </ModalLayout>

        <ModalLayout isShow={change_status === Status.SUCCEEDED}>
        <HomeFormContainer>
          <FormTitle>Client accepted</FormTitle>
          <ButtonSubmit title="Close" onClick={handlerCloseAccept} />
        </HomeFormContainer>
        </ModalLayout>

      </DashboardWrapper>
  );
};

export default DashboardPayments;
