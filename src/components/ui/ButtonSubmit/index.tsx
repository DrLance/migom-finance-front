import React from 'react';
import { BtnSubmitElement } from "./style";

interface Props {
  title: string;
  onClick: () => void;
  isDisable?: boolean;
}

const ButtonSubmit: React.FunctionComponent<Props> = (props) => {
  return (
      <BtnSubmitElement onClick={props.onClick} disabled={props.isDisable}>
        {props.title}
      </BtnSubmitElement>
  );
};

export default ButtonSubmit;
