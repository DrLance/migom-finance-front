import { email as emailReg } from "./regex";

export const validateEmail = (email: string): boolean => {
  return emailReg.test(email);
}
