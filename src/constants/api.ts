export const baseUrl = process.env.REACT_APP_API_URL || "http://127.0.0.7"; //https://mfb.coderman.ru

export const mePath = "/api/user";
export const registerPath = "/api/register";
export const loginPath = "/api/login";
export const logoutPath = "/api/logout";
export const resetPasswordPath = "/api/reset-password";
export const newPasswordPath = "/api/new-password";
export const productsPath = "/api/products";
export const countriesPath = "/api/countries";
export const userSetParamPath = "/api/user/set-param";
export const userAddLeadPath = "/api/user/add-lead";
export const userGetLeadsPath = "/api/user/get-leads";
export const userGetLeadsPaymentsPath = "/api/user/get-leads-payments";
export const userChangeStatusPath = "/api/user/change-status";
export const userChangeBalancePath = "/api/user/change-balance";
export const leadChangeStatusPath = "/api/leads/change-status";
export const leadAcceptPath = "/api/lead/accept";
export const leadDeclinePath = "/api/lead/decline";
export const adminGetAgentPath = "/api/admin/get-agents";
export const adminGetPaymentSystemsPath = "/api/admin/get-payment-systems";

export const setUserToken = ( value: string) => localStorage.setItem("migom_finance_access_token", value);
export const getUserToken = () => localStorage.getItem("migom_finance_access_token") || "";
export const removeUserToken = () => localStorage.removeItem("migom_finance_access_token");
