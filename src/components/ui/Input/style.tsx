import styled, {keyframes} from "styled-components";
import { poppinsFont } from "../../../styles/fonts";
import { black, blue, grey } from "../../../styles/colors";

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%
`

export const InputRow = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  box-sizing: border-box;
`

export const InputLabel = styled.label`
  font-family: ${poppinsFont};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${black};
  margin-bottom: 12px;
`

export const InputElement = styled.input<{ isError?: boolean }>`
  font-family: ${poppinsFont};  
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;  
  border: 1px solid rgba(127, 127, 127, 0.3);
  box-sizing: border-box;
  border-radius: 6px;
  width: 100%; 
  color: ${black};
  height: 46px;
  padding: 13px;
  box-shadow: ${ props => props.isError ? "0 0 0 2px rgba(146, 157, 194, 0.3)" : "none" } ;  
  outline: none;
  transition: all 0.3s cubic-bezier(0.497, 0.51, 0.25, 1) 0s;
  
  :focus {
    box-shadow: 0 0 0 2px rgba(146, 157, 194, 0.3);   
  }
  
  ::placeholder {
    color: ${grey};
  }
`

export const InputForgotLink = styled.a`
  font-family: ${poppinsFont};
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${blue};
  text-decoration: underline;
  
  :hover {
    text-decoration: none;
  }
  
`

const Slide = keyframes`
  0% { left: -24px; }
  100% { left: 0;}
`;

export const InputErrorText = styled.a`
  position: absolute;
  bottom: -24px;
  font-family: ${poppinsFont};
  font-weight: normal;  
  font-size: 12px;
  line-height: 20px;
  color: #FE2828;
  margin: 0;
  margin-top: 6px;
  animation: ${Slide};
  animation-duration: 0.3s;
  animation-fill-mode: forwards;
`


