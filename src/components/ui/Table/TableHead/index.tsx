import React from 'react';
import { TableHeadContainer } from "./style";

const TableHeadRow: React.FunctionComponent = (props) => {
  return (
      <TableHeadContainer>
        {props.children}
      </TableHeadContainer>
  );
};

export default TableHeadRow;
