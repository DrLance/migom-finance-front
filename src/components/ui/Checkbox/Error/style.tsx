import styled from "styled-components";
import errorWarningIcon from "../../../../assets/error-warning.svg";
import { poppinsFont } from "../../../../styles/fonts";

export const CheckboxErrorContainer = styled.div`
  display: flex;
  align-items: center;
  background: rgba(254, 40, 40, 0.1);
  border-radius: 6px;
  padding: 6px 12px;
  margin-bottom: 36px;
  p {
    font-family: ${poppinsFont};
    font-size: 12px;
    line-height: 20px;
    color: #FE2828;
    margin: 0;
    margin-left: 6px;    
  }
  
`

export const CheckboxErrorIcon = styled.img.attrs({
  src: errorWarningIcon,
  alt: "Checkbox not fill"
})`
`
