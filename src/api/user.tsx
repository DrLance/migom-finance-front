import axios, { AxiosRequestConfig } from "axios";
import {
  baseUrl, mePath, productsPath, countriesPath, userSetParamPath, userAddLeadPath, userGetLeadsPath, userGetLeadsPaymentsPath,
  leadChangeStatusPath, adminGetPaymentSystemsPath, adminGetAgentPath, userChangeStatusPath, userChangeBalancePath
} from "../constants/api";
import { getAuthHeader } from "./auth";
import { UserLeadData, UserParamData } from "../types";

export const getUserApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${mePath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}


export const getProductsApi = async () => {
  try {
    const response = await axios.post(`${baseUrl}${productsPath}`);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}


export const getCountriesApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${countriesPath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export const setParamApi = async (token: string, data: UserParamData) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${userSetParamPath}`, data, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export const userAddLeadApi = async (data: UserLeadData) => {

  try {
    const response = await axios.post(`${baseUrl}${userAddLeadPath}`, data);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}


export const getUserLeadsApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${userGetLeadsPath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export const getUserPaymentLeadsApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${userGetLeadsPaymentsPath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}


export const leadChangeStatusApi = async (token: string, status: string, id: string | number) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${leadChangeStatusPath}`, { status, id }, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}


export const adminGetAgentsApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${adminGetAgentPath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export const adminGetPaymentSystemsApi = async (token: string) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${adminGetPaymentSystemsPath}`, {}, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export type ChangeStatus = {
  user_id: number | string,
  status: string
}

export const userChangeStatusApi = async (token: string, data: ChangeStatus) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${userChangeStatusPath}`, data, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}

export type ChangeBalance = {
  user_id: number | string,
  balance: string | number,
  type: string | number,
}

export const userChangeBalanceApi = async (token: string, data: ChangeBalance) => {

  const config: AxiosRequestConfig = {
    headers: getAuthHeader(token),
  };

  try {
    const response = await axios.post(`${baseUrl}${userChangeBalancePath}`, data, config);

    return response.data.data;

  } catch (err) {
    return await Promise.reject("Failed get data!");
  }
}
