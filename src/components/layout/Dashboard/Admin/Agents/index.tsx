import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { adminGetAgents, userChangeBalance, userChangeStatus } from "../../../../../slices/userSlice";
import { RootState } from "../../../../../store";
import DashboardNav from "../../Navigation";
import { DashboardWrapper, FormSubTitle, FormTitle, HomeFormContainer, ModalLayout } from "../../../../../styles";
import Table from "../../../../ui/Table";
import { TableActions, TableActionsItem } from "../../../../ui/Table/style";
import useComponentVisible from "../../../../../constants/hooks";
import Switcher from "../../../../ui/Switch";
import Input from "../../../../ui/Input";
import ButtonSubmit from "../../../../ui/ButtonSubmit";
import { InputLabel } from "../../../../ui/Input/style";

const TABLE_TITLES = [
  {
    title: "#",
    field_name: "index"
  },
  {
    title: "Date Created",
    field_name: "created_at"
  },
  {
    title: "Name",
    field_name: "name"
  },
  {
    title: "Email",
    field_name: "email"
  }, {
    title: "Status",
    field_name: "status"
  },
  {
    title: "Balance",
    field_name: "balance"
  },
  {
    title: "",
    field_name: "actions"
  },
];

const SWITCH_ITEMS = [
  {
    id: 1,
    name: "Top Up",
  },
  {
    id: 2,
    name: "Charge",
  }
];

const DashboardAdminAgents = () => {
  const dispatch = useDispatch();

  const [showTransfer, setShowTransfer] = useState(false);
  const [actionsObj, setActionsObj] = useState({ x: 0, y: 0, id: 0 });
  const [currentStatus, setCurrentStatus] = useState('reg');
  const [showStatus, setShowStatus] = useState(false);
  const [typeBalance, setTypeBalance] = useState(1);
  const [amount, setAmount] = useState("0");

  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  useEffect(() => {
    dispatch(adminGetAgents());
  }, [dispatch])

  const { admin_data } = useSelector((state: RootState) => state.user);

  const handlerSubmit = async () => {

    await dispatch(userChangeBalance({user_id: actionsObj.id, balance: amount, type: typeBalance}));
    await dispatch(adminGetAgents());

    setShowTransfer(false);
  }

  const handleChange = async () => {
    await dispatch(userChangeStatus({ status: currentStatus, user_id: actionsObj.id }));
    await dispatch(adminGetAgents());

    setShowStatus(false);
  }

  const handleChangeStatus = () => {
    setShowStatus(true)
  }

  const amountChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const value = e.currentTarget.value;

    setAmount(value);
  }


  const handleCurrentStatus = (e: React.SyntheticEvent<HTMLSelectElement>) => {
    const value = e.currentTarget.value;

    setCurrentStatus(value);
  }

  const findItem = admin_data.filter((item: any) => item.id === actionsObj.id )[0]

  return (
      <div style={{ display: "flex" }}>
        <DashboardNav/>
        <DashboardWrapper style={{ padding: "60px", marginLeft: "0", marginRight: "24px" }}>
          <div style={{ width: "100%", display: "flex", overflow: "auto" }}>
            <Table
                headTitles={TABLE_TITLES}
                tableItems={admin_data ? admin_data : []}
                onClickRow={() => {
                }}
                onClickCellAction={(e, id) => {
                  const { left, top } = e.currentTarget.getBoundingClientRect();
                  setIsComponentVisible(true);

                  setActionsObj({ x: left, y: top, id });
                }}
            />
          </div>
        </DashboardWrapper>

        <ModalLayout isShow={showTransfer}>
          <HomeFormContainer>
            <FormTitle>{findItem ? findItem.name : ""}</FormTitle>
            <FormSubTitle>Avaliable: $ {findItem ? findItem.balance : ""} </FormSubTitle>
            <Switcher onCallbackChange={(item) => setTypeBalance(item)} items={SWITCH_ITEMS}/>
            <div style={{marginBottom: "24px" }}/>
            <Input labelText="Amount, $" placeholder="" type="text" onChange={amountChange}/>
            <div style={{marginBottom: "36px" }}/>
            <ButtonSubmit title="Submit" onClick={handlerSubmit}/>
          </HomeFormContainer>
        </ModalLayout>

        <ModalLayout isShow={showStatus}>
          <div style={{ margin: "0 auto" }}>
            <HomeFormContainer>
              <FormTitle>Change agents status</FormTitle>
              <InputLabel>Change to</InputLabel>
              <select style={{ marginBottom: "36px"}} onChange={handleCurrentStatus}>
                <option value="reg">Client started registration</option>
                <option value="approve">Client approved</option>
                <option value="wait">Wayting for payment institution</option>
              </select>

              <ButtonSubmit title="Change" onClick={handleChange} />

            </HomeFormContainer>
          </div>
        </ModalLayout>



        <TableActions
            ref={ref}
            isVisible={isComponentVisible}
            x={actionsObj.x}
            y={actionsObj.y}
        >
          <TableActionsItem onClick={() => setShowTransfer(true)}>
            Balance
          </TableActionsItem>
          <TableActionsItem onClick={handleChangeStatus}>
            Сhange agent status
          </TableActionsItem>
        </TableActions>

      </div>
  );
};

export default DashboardAdminAgents;
