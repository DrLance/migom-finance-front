import styled  from "styled-components";
import { poppinsFont } from "../../../styles/fonts";
import { lightLightBlue } from "../../../styles/colors";
import dotsImg from "../../../assets/dots.svg";
import dotsImgHover from "../../../assets/dots-hover.svg";

export const TableContainer = styled.table<{ showActions?: boolean }>`
  border-collapse: collapse;
  font-family: ${poppinsFont};
  overflow: scroll;
  white-space: nowrap;
  width: 100%;
  margin-bottom: 20px;

  thead, tr {
    border-bottom: 1px solid rgba(146, 157, 194, 0.2);
  }

  tbody {
    tr {
      height: 53px;


      :nth-child( even ) {
        background: rgba(233, 235, 243, 0.2);

        :hover {
          background: rgba(224, 232, 254, 0.5);
        }
      }

      :hover {
        background: rgba(224, 232, 254, 0.5);
      }
    }
  }

  thead {
    th {
      :last-of-type {
        padding-right: ${props => props.showActions ? "48px" : "24px"};
      }
    }
  }
`


export const TableActions = styled.div<{ isVisible: boolean, x:number, y: number}>`
  position: absolute;  
  top: ${props => props.y + "px" };
  left: ${props => (props.x + 24) + "px" };
  display: ${props => props.isVisible ? "flex" : "none"};
  align-items: flex-start;
  flex-direction: column;
  background: #FFFFFF;
  box-shadow: 6px 6px 24px rgba(37, 59, 134, 0.11);
  border-radius: 12px;
  padding: 12px; 
     
`

export const TableActionsItem = styled.p`
  font-family: ${poppinsFont};
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: right;
  color: ${lightLightBlue};
  padding: 0;
  margin: 0;
  margin-bottom: 6px;

  :last-of-type {
    margin-bottom: 0;
  }
  
  :hover {
    cursor: pointer;
  }
`

export const TableCellActions = styled.div`  
  height: 24px;
  width: 4px;
  background-image: url("${dotsImg}");
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;  
  
  :hover {
    cursor: pointer;
    background-image: url("${dotsImgHover}");
  }
`
