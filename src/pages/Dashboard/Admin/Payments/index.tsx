import React from 'react';
import { useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { DashboardContent, DashboardLayout } from "../../../../styles";
import DashboardHeader from "../../../../components/layout/Dashboard/Header";
import DashboardAdminPayments from "../../../../components/layout/Dashboard/Admin/Payments";

const DashboardAdminPaymentsPage = () => {
  const {data: { name}} = useSelector((state: RootState) => state.user);
  return (
      <DashboardLayout>
        <DashboardContent>
          <DashboardHeader refLink={null} name={name}/>
          <DashboardAdminPayments />
        </DashboardContent>
      </DashboardLayout>
  );
};

export default DashboardAdminPaymentsPage;
