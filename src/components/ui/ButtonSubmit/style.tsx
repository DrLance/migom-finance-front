import styled from "styled-components";
import { globalFont } from "../../../styles/fonts";
import { orange } from "../../../styles/colors";

export const BtnSubmitElement = styled.button`
  font-family: ${globalFont};
  border-radius: 8px;
  font-weight: 600;
  font-size: 16px;
  line-height: 28px;
  text-align: center;
  color: #FFFFFF;  
  background-color: ${orange};
  width: 100%;
  height: 56px;
  border: none;
  outline: none;
  box-sizing: border-box;
  
  :disabled {
    background: rgba(254, 87, 40, 0.3);
  }
  
  :hover:not(:disabled) {
    background: #E64F24;
  }
`
